SQLAlchemy>=0.8.3
alembic>=1.8.0
beaker
beautifulsoup4
cryptography>=3.0 # BSD/Apache-2.0
docutils>=0.11
oslo.config>=1.6.0 # Apache-2.0
oslo.db>=1.4.1 # Apache-2.0
oslo.log>=3.11.0
oslo.utils>=3.16.0 # Apache-2.0
pecan>=0.8.2
requests>=2.2.0,!=2.4.0
requests-cache>=0.4.9,<0.6.0
jsonschema>=4.7.0
PyJWT>=2.0.0  # MIT
WebOb>=1.7.1  # MIT
PyMySQL>=0.6.2,!=0.6.4
