coverage>=3.6
hacking>=3.0.1,<3.1.0;python_version>='3.5' # Apache-2.0
pycodestyle>=2.0.0,<2.6.0 # MIT
flake8-import-order==0.11 # LGPLv3

docutils>=0.11  # OSI-Approved Open Source, Public Domain
httmock>=1.2.4
oslotest>=1.2.0 # Apache-2.0
python-subunit>=0.0.18
stestr>=1.1.0 # Apache-2.0
testtools>=0.9.34
PyMySQL>=0.6.2,!=0.6.4
WebTest>=3.0.0
