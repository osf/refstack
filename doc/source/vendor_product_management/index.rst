Vendor and product management
=============================

RefStack has implemented a vendor and product registration process so that test
results can be associated to products of vendors. The creation and management
of vendor and product entities can be done using the RefStack Server UI or
RefStack APIs. The following is a quick guide outlining the information related
to the creation and management of those entities.

.. toctree::
   :maxdepth: 1
   :includehidden:

   VendorEntity
   ProductEntity
