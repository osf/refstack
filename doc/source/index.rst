.. Refstack documentation master file, created by
   sphinx-quickstart on Fri Aug  5 01:41:59 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

====================================
Welcome to RefStack's documentation!
====================================

Content:
--------
.. toctree::
   :maxdepth: 2

   refstack_setup_via_ansible
   overview
   contributing
   refstack
   vendor_product_management/index
   uploading_private_results
   test_result_management
